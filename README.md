# Code Checker #

Code checker script provides the way to check the quality of the [Interitty](https://gitlab.com/interitty/) codes.

## Requirements ##

- [PHP](https://php.net/) >= 8.3

## Installation ##

The best way to install [**interitty/code-checker**](https://gitlab.com/interitty/code-checker) is using [Composer](https://getcomposer.org/):

```bash
composer require --dev interitty/code-checker
```

To install the git hooks, simply run the following command in the root dir of the project (*where `.git` and `vendor` folders are located*):

```bash
./vendor/bin/git-hook --install
```

For macOS users, it is necessary to unify the `coreutils` with the Linux users simply by running the following command:

```bash
brew install coreutils
```

## Usage ##

If the global PATH property contains a path to the folder where the `code-check` script is, it is possible
to run the command directly in the project folder without any arguments and check if there is an error in the code.

```bash
code-checker
```

Optionally the project folder can be specified by the relative or absolute path

```bash
code-checker --ignore-path=./interitty/project/vendor ./interitty/project
```

More than one path can also be checked by concatenating the `--path` parameter.

```bash
code-checker --base-dir=./interitty/project --path=./src --path=./tests
```

The `code-checker` will return the lowest exit code that happens thru execution. When the exit code is `0`, everything
is OK in the project. This was used in the `git-hook`, which can be used as git [pre-commit](https://git-scm.com/book/uz/v2/Customizing-Git-Git-Hooks),
[commit-msg](https://git-scm.com/book/uz/v2/Customizing-Git-Git-Hooks), [pre-push](https://git-scm.com/book/uz/v2/Customizing-Git-Git-Hooks)
hook, or in the CI-CD pipeline.

The `git-hook` used as [commit-msg](https://git-scm.com/book/uz/v2/Customizing-Git-Git-Hooks) also checks the commit
message and allows to skip checks for "*work in progress*" commit where the message starts on `WIP:` or simply `.`,
but not for pushing into protected branches (*`master`, `test`, `develop`*).

## Checkers setup ##

In some cases, like when a new version of PHP comes, it can be useful to set up the list of enabled checkers.
Because of that, there is an optional `--checks=<CHECKS>` parameter that allows specifying the enabled checks by
a comma-separated list of their names. The default value for the list contains all of them.

```bash
code-checker --checks=static_analysis,mess_detector,code_sniffer,coding_standard_fixer,phpunit
```

An alternative way, useful for CI-CD pipelines, is to set up the `CHECKS` environment variable.

```bash
CHECKS=static_analysis,mess_detector,code_sniffer,coding_standard_fixer,phpunit code-checker
```

Due to a [PHP8 Unexpected token bug](https://github.com/phpmd/phpmd/issues/853) can be handy to set up globally
a `CHECKS` environment variable to disable a `mess_detector`, for example in `~/.bashrc file`.

## Settings ##

There are some more settings you can need to fit your suit.

| Script parameter                    | Description                                                                                  |
| ----------------------------------- | -------------------------------------------------------------------------------------------- |
| `--base-dir="…"`                    | The base directory of all paths, which is also used for truncating output messages.          |
| `--bin-dir="…"`                     | Path of the composer bin dir where are all executables located.                              |
| `--check_arguments_phpcs="…"`       | Optional arguments for PHP CodeSniffer.                                                      |
| `--check_arguments_phpcsfixer="…"`  | Optional arguments for PHP Coding Standards Fixer.                                           |
| `--check_arguments_phpmd="…"`       | Optional arguments for PHP Mess detector.                                                    |
| `--check_arguments_phpstan="…"`     | Optional arguments for PHP Static Analysis Tool.                                             |
| `--check_arguments_phpunit="…"`     | Optional arguments for PHP Unit.                                                             |
| `--checks=<CHECKS>`                 | Comma-separated list of enabled checks (static_analysis,mess_detector,code_sniffer,phpunit). |
| `--color`                           | Enable color output.                                                                         |
| `--colors="…"`                      | Specify the colour output in detail.                                                         |
| `--vv`, `--debug`                   | Increase verbosity level to show also debug messages.                                        |
| `--fail-fast`                       | Stop processing other tests when the first error happens.                                    |
| `--generate-baseline`               | Generate PHPStan baseline file for the current project.                                      |
| `--fix`                             | Try to fix automatically what is possible.                                                   |
| `-h`, `--help`                      | Show the help message.                                                                       |
| `--ignore-path="…"`                 | The path that should be excluded from checks.                                                |
| `--memory-limit="…"`                | Specifies the memory limit in the same format php.ini accepts.                               |
| `--no-color`                        | Disable color output.                                                                        |
| `--no-output`                       | An alias for `--quiet`.                                                                      |
| `--path="…"`                        | The optional way to specify the path for files or folders for processing.                    |
| `-q`, `--quiet`                     | Decrease verbosity level to 0 to hide all output.                                            |
| `-v`, `--verbose`                   | Increase the verbosity level so that warning messages are also displayed.                    |
| `--xdebug`                          | Allow running with Xdebug for debugging purposes.                                            |

## Environment variables ##

Some of the settings can also be set by the environment variable.

| Environment variable                | Description                                                                                  |
| ----------------------------------- | -------------------------------------------------------------------------------------------- |
| `CHECK_ARGUMENTS_PHPCS"…"`          | Optional arguments for PHP CodeSniffer.                                                      |
| `CHECK_ARGUMENTS_PHPCSFIXER"…"`     | Optional arguments for PHP Coding Standards Fixer.                                           |
| `CHECK_ARGUMENTS_PHPMD"…"`          | Optional arguments for PHP Mess detector.                                                    |
| `CHECK_ARGUMENTS_PHPSTAN"…"`        | Optional arguments for PHP Static Analysis Tool.                                             |
| `CHECK_ARGUMENTS_PHPUNIT"…"`        | Optional arguments for PHPUnit.                                                              |
| `CHECKS="…"`                        | Comma-separated list of enabled checks (static_analysis,mess_detector,code_sniffer,phpunit). |
| `CLICOLOR=1`                        | Enable color output.                                                                         |
| `DEBUG=1`                           | Increase verbosity level to show also debug messages.                                        |
| `DOCKER_ENVS="…"`                   | Comma-separated list of environment variable names to promote them to the docker.            |
| `GLOBAL_BASELINE="…"`               | Path where the PHPStan baseline file is located.                                             |
| `MEMORY_LIMIT="…"`                  | Memory limit for PHP Static Analysis Tool analysis.                                          |
| `NO_OUTPUT=1`                       | Decrease verbosity level to 0 to hide all output.                                            |
| `VERBOSE=1`                         | Increase the verbosity level so that warning messages are also displayed.                    |

## Docker support ##

If a global environment `DOCKER_DEV_CONTAINER` is set up and a docker container with the setup `<name>` settings
is started, the `code-checker` and `git-hook` commands are executed in it. To set the global environment value
"*permanently*" just add the following command into the `.bashrc` file or the similar file related to the shell
you are using.

```bash
export DOCKER_DEV_CONTAINER="<name>"
```

The `docker-exec` script is used to run the command in the defined docker container.

```bash
docker-exec "/path/command --some-arguments /path/to/project"
```

## Fix IDE ##

The IDEs help developers in many cases, but sometimes they are not so useful. In the integration between the IDEs and
third-party applications, they provide the address where the project's properties file is located instead of the project
address. Less complicated but still not very useful is that these applications run from the root folder instead of
the project folder.

For these cases, there is a `fix-ide` script that detects the address containing the `nbproject` folder with the project
properties and changes them according to the project directory.

For example the following call will detect existence of the `/path/to/project-properties/nbproject` folder and call
the given command with the project folder, that is defined as the `src.dir` property in the `./nbproject/project.properties`
file.

```bash
#$PWD = /
fix-ide /path/command --some-arguments /path/to/project-properties
```

Previous command will lead to call the following one.

```bash
#$PWD = /path/to/project
/path/command --some-arguments /path/to/project
```

### PHP Mess Detector ###

To run the `phpmd` from the IDE in the docker container with the custom ruleset, use the same-named `phpmd` script from
the [**interitty/code-checker**](https://gitlab.com/interitty/code-checker) instead of the original one.

![PHP Mess Detector configuration example](assets/phpmd-configuration-example.png)

For example, when the [**interitty/code-checker**](https://gitlab.com/interitty/code-checker) is cloned in
`/opt/srv/interitty/interitty/code-checker` folder, docker container is named `dev-php-fpm` and prefered ruleset is
in `./src/phpmd/rulesets/interitty.xml`, add following string into NetBeans config.

```config
/opt/srv/interitty/interitty/code-checker/bin/phpmd --color --docker-container=dev-php-fpm --fix-ide --force-ruleset=/opt/srv/interitty/interitty/code-checker/src/phpmd/rulesets/interitty.xml
```

### PHP CodeSniffer ###

To run the `phpcs` from the IDE in the docker container with the custom ruleset, use the same-named `phpcs` script from
the [**interitty/code-checker**](https://gitlab.com/interitty/code-checker) instead of the original one.

![PHP CodeSniffer configuration example](assets/phpcs-configuration-example.png)

For example, when the [**interitty/code-checker**](https://gitlab.com/interitty/code-checker) is cloned in
`/opt/srv/interitty/interitty/code-checker` folder, docker container is named `dev-php-fpm` and prefered ruleset is
in `./src/phpcs/rulesets/interitty`, add following string into NetBeans config.

```config
/opt/srv/interitty/interitty/code-checker/bin/phpcs --color --docker-container=dev-php-fpm --fix-ide --force-ruleset=/opt/srv/interitty/interitty/code-checker/src/phpcs/rulesets/interitty
```

### PHP Coding Standards Fixer ###

To run the `php-cs-fixer` from the IDE in the docker container with the custom ruleset, use the same-named `php-cs-fixer`
script from the [**interitty/code-checker**](https://gitlab.com/interitty/code-checker) instead of the original one.

![PHP Coding Standards Fixer configuration example](assets/php-cs-fixer-configuration-example.png)

For example, when the [**interitty/code-checker**](https://gitlab.com/interitty/code-checker) is cloned in
`/opt/srv/interitty/interitty/code-checker` folder, docker container is named `dev-php-fpm` and prefered ruleset is
in `/src/php-cs-fixer/.php-cs-fixer.php`, add following string into NetBeans config.

```config
/opt/srv/interitty/interitty/code-checker/bin/php-cs-fixer
```

And following string into `Default Options`.

```config
--color --docker-container=dev-php-fpm --fix-ide --config=/opt/srv/interitty/interitty/code-checker/src/php-cs-fixer/.php-cs-fixer.php
```

### PHP Static Analysis Tool ###

To run the `phpstan` from the IDE in the docker container with the custom ruleset, use the same-named `phpstan` script
from the [**interitty/code-checker**](https://gitlab.com/interitty/code-checker) instead of the original one.

![PHP Static Analysis Tool](assets/phpstan-configuration-example.png)

For example, when the [**interitty/code-checker**](https://gitlab.com/interitty/code-checker) is cloned in
`/opt/srv/interitty/interitty/code-checker` folder, docker container is named `dev-php-fpm` and prefered ruleset is
in `/src/phpstan/interitty.neon`, add following string into NetBeans config.

```config
/opt/srv/interitty/interitty/code-checker/bin/phpstan --color --docker-container=dev-php-fpm --fix-ide
```

And following string into `Configuration`.

```config
/opt/srv/interitty/interitty/code-checker/src/phpstan/interitty.neon
```

Occasionally it can be useful to be able to add something to ignore in [PHPStan](https://phpstan.org) so that it is not part
of the project, since the cause is already resolved and just not yet part of the current version. In this case, it is possible
to generate a [PHPStan baseline](https://phpstan.org/user-guide/baseline) file into the project root or home folder.