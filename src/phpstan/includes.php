<?php

$files = [
    '~/phpstan-global.neon',
    '~/phpstan-baseline.neon',
    __DIR__ . '/../../phpstan-baseline.neon',
    __DIR__ . '/../../../../../phpstan-baseline.neon',
];

$globalBaselineFile = getenv('GLOBAL_BASELINE');
if ($globalBaselineFile !== false) {
    if (file_exists($globalBaselineFile) === true) {
        $content = file_get_contents($globalBaselineFile); // @phpstan-ignore-line
    } else {
        // Fix https://gitlab.com/gitlab-org/gitlab/-/issues/29407
        $content = $globalBaselineFile;
    }
    $globalBaselineFile = sys_get_temp_dir() . '/GLOBAL_BASELINE.neon';
    file_put_contents($globalBaselineFile, $content); // @phpstan-ignore-line
    $files[] = $globalBaselineFile;
}

$includes = [];
foreach ($files as $file) {
    if (file_exists($file) === true) {
        $includes[] = realpath($file);
    }
}

return [
    'includes' => $includes,
];
