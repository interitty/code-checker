<?php

declare(strict_types=1);

use PhpCsFixer\Fixer\Basic\BracesFixer;
use PhpCsFixer\Fixer\Import\OrderedImportsFixer;

// <editor-fold defaultstate="collapsed" desc="Helpers">
/**
 * Fix paths to be compatible with Php Coding Standards Fixer
 *
 * @param string[] $paths
 * @param string $baseDir
 * @return array
 */
function fixPaths(array $paths, string $baseDir): array
{
    $fixPaths = [];
    foreach ($paths as $key => $path) {
        $fixPaths[$key] = fixPath($path, $baseDir);
    }
    return $fixPaths;
}

/**
 * Fix paths to be compatible with Php Coding Standards Fixer
 *
 * @param string $path
 * @param string $baseDir
 * @return string
 */
function fixPath(string $path, string $baseDir): string
{
    $relativePath = relativePath($path, $baseDir);
    $fixPath = preg_replace('~^\./~', '', $relativePath);
    return $fixPath ?? $path;
}

/**
 * Convert given absolute path to realtive to given base dir
 *
 * @param string $absolutePath
 * @param string $baseDir
 * @return string
 */
function relativePath(string $absolutePath, string $baseDir): string
{
    $relativePath = preg_replace('~^' . $baseDir . '~', '.', $absolutePath);
    return $relativePath ?? $absolutePath;
}
// </editor-fold>
$baseDir = (string) rtrim(getenv('BASE_DIR') ?: '', '/' . DIRECTORY_SEPARATOR);
$checkPaths = fixPaths(explode(',', (string) getenv('CHECK_PATH')), $baseDir);
$ignorePaths = fixPaths(explode(',', (string) getenv('IGNORE_PATHS')), $baseDir);


$finder = PhpCsFixer\Finder::create()
    ->exclude('somedir')
    ->notPath('src/Symfony/Component/Translation/Tests/fixtures/resources.php')
    ->in(__DIR__)
;

$config = new PhpCsFixer\Config();
$config->setRules([
    '@PSR12' => true,
    'binary_operator_spaces' => true,
    'braces' => [
        'allow_single_line_anonymous_class_with_empty_body' => true,
        'allow_single_line_closure' => false,
        'position_after_anonymous_constructs' => BracesFixer::LINE_SAME,
        'position_after_control_structures' => BracesFixer::LINE_SAME,
        'position_after_functions_and_oop_constructs' => BracesFixer::LINE_NEXT,
    ],
    'concat_space' => ['spacing' => 'one'],
    'no_extra_blank_lines' => false,
    'no_singleline_whitespace_before_semicolons' => true,
    'no_whitespace_before_comma_in_array' => true,
    'space_after_semicolon' => [
        'remove_in_empty_for_expressions' => true,
    ],
    'unary_operator_spaces' => true,
    'whitespace_after_comma_in_array' => true,
]);
$config->setFinder($finder);
return $config;
