# Changelog #
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased] ##

### Added ###

### Changed ###

### Fixed ###

## [1.0.11] - 2025-01-30 ##

### Added ###

- Support to set `MEMORY_LIMIT` environment variable for PHP Static Analysis Tool

### Changed ###

- Xdebug disabled by default until `--xdebug` argument is passed

## [1.0.10] - 2024-12-07 ##

### Changed ###

- Upgrade dependent packages
- Upgrade license to 2025

## [1.0.9] - 2024-08-30 ##

### Added ###

- Add universalObjectCratesClasses

### Changed ###

- Update uncheckedExceptionClasses
- Upgrade dependent packages

### Fixed ###

- Phpstan extension.neon of composer/pcre missing by dg/composer-cleaner

## [1.0.8] - 2024-05-19 ##

### Changed ###

- Update SECURITY key
- Upgrade dependent packages

## [1.0.7] - 2023-12-28 ##

### Changed ###

- Increase minimal PHP to 8.3
- Update security contacts
- Upgrade dependent packages
- Upgrade license to 2024

## [1.0.6] - 2023-03-12 ##

### Changed ###

- Ignore missing @throws in tests
- Slevomat CodingStandard - Functions.StaticClosure
- Slevomat CodingStandard - Functions.UnusedInheritedVariablePassedToClosure
- Replace pepakriz/phpstan-exception-rules
- Suggest \Interitty\Utils\Filesystem
- Upgrade dependent packages

## [1.0.5] - 2023-01-29 ##

### Added ###

- PHPStan global ignore support

## [1.0.4] - 2023-01-26 ##

### Added ###

- PhpStan deprecation rule

### Changed ###

- Upgrade dependent packages

## [1.0.3] - 2022-12-29 ##

### Changed ###

- Upgrade license to 2023
- Increase minimal PHP to 8.2
- Upgrade dependent packages

## [1.0.2] - 2022-09-28 ##

### Changed ###

- Unchecked ExtendedExceptionInterface

## [1.0.1] - 2022-09-15 ##

### Changed ###

- Update dependencies to newer version
- Enable Static Analysis checkExplicitMixedMissingReturn

## [1.0.0] - 2022-07-20 ##

### Added ###

- Check script basics
- Checks list support
- Codesniffer basics
- Coding Standard Fixer basics
- Docker support
- Git hook
- Mess Detector Basics
- PHPUnit support
- Static Analysis Tool Basics
- Use checker
